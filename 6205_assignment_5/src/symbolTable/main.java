package symbolTable;

import java.util.Random;

public class main {

	public static void main(String[] args) {
		//N & M & X
		 final int N = 1000;
		 final int M = 2*N;
		 final int X = 10*N;
		 final int loopTimes = 1;

		 double depthAveAfter = 0;
		 double depthMaxAfter = 0;
		 
		 for(int k= 0 ; k < loopTimes ; k++) {
			 
			//Initiate the tree
			 BSTSimple<String, Integer> bst = new BSTSimple<>();
			 while(bst.size() < N) {
				 int temp = (int)(Math.random()*M);
				 bst.put(Integer.toString(temp), 1);
			 }
			 
			 //Insert & delete
			 for(int i = 0 ; i < X ; i++) {
				 Random randomB = new Random();
				 boolean insertOrDelete = randomB.nextBoolean();
				 int tempInt = (int)(Math.random()*M);
				 String temp = Integer.toString(tempInt);
				 if(insertOrDelete) 
					 bst.put(temp, 1);
				 if(!insertOrDelete) 
					 bst.delete(temp);
			 }
			 
			 depthAveAfter = (depthAveAfter * k + bst.depthAve())/(k+1);
			 depthMaxAfter = (depthMaxAfter * k + bst.depthMax())/(k+1);
		 }
		 	System.out.println("N : " + N);
		 	System.out.println("Ave After : " + depthAveAfter);
		 	System.out.println("Max After : " + depthMaxAfter);
	}

}
